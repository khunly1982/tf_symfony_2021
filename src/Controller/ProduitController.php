<?php

namespace App\Controller;

use App\Entity\Produit;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProduitController extends AbstractController
{
    private $produits;

    public function __construct()
    {
        $this->produits = [
            (new Produit())->setPromotion(10)
                ->setImage('https://s1.medias-auto5.be/images_produits/650x650/79177.jpg')
                ->setNom('Coca cola')
                ->setPrix(0.7),
            (new Produit())->setPromotion(null)
                ->setImage('https://yourspanishcorner.com/6660-thickbox_default/fanta-orange-canette-33-cl.jpg')
                ->setNom('Fanta')
                ->setPrix(0.7),
            (new Produit())->setPromotion(20)
                ->setImage('https://pizzacheztoi.com/wp-content/uploads/2018/07/sprite.png')
                ->setNom('Sprite')
                ->setPrix(0.7),
            (new Produit())->setPromotion(null)
                ->setImage('https://www.myamericanmarket.com/977-large_default/dr-pepper-soda.jpg')
                ->setNom('Dr Pepper')
                ->setPrix(1),
            (new Produit())->setPromotion(null)
                ->setImage('https://cdn.carrefour.eu/1200_05563975_5060466511859_02.jpeg')
                ->setNom('Nalu')
                ->setPrix(1.5),
        ];
    }

    /**
     * @Route("/produit", name="produit_index")
     */
    public function index(): Response
    {
        return $this->render('produit/index.html.twig', ['produits' => $this->produits]);
    }
}
