<?php

namespace App\Controller;

use App\Entity\Produit;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route(path="/", name="default_index")
     */
    public function index(): Response
    {
        // traitement à faire par le controller
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route(path="/contact", name="default_contact")
     */
    public function contact(): Response
    {
        return $this->render('default/contact.html.twig');
    }

    /**
     * @Route(path="/variables", name="default_variables")
     */
    public function variables(): Response
    {
        $chaine = 'Ma Chaine de caractères';
        $nombre = 42.424245;
        $date = new \DateTime();
        $tableau = ['sel', 'poivre', 'sucre'];
        $isConnected = true;
        $produit = new Produit();
        $produit->setPromotion(10)
            ->setImage('https://fr.coca-cola.be/content/dam/one/be/fr/products/coca-cola-original-taste-fr.png')
            ->setNom('Coca cola')
            ->setPrix(0.7);

        return $this->render('default/variables.html.twig', [
            'chaine' => $chaine,
            'nombre' => $nombre,
            'date' => $date,
            'tableau' => $tableau,
            'isConnected' => $isConnected,
            'produit' => $produit
        ]);
    }
}
