<?php

namespace App\Controller;

use App\Entity\Produit;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ProduitCorrectionController extends AbstractController
{
    private $db;

    public function __construct()
    {
        $this->db = [
            (new Produit())->setId(1)->setNom('Coca Cola')->setPrix(0.7)->setImage('https://s1.medias-auto5.be/images_produits/650x650/79177.jpg')->setPromotion(10),
            (new Produit())->setId(2)->setNom('Sprite')->setPrix(0.75)->setImage('https://pizzacheztoi.com/wp-content/uploads/2018/07/sprite.png')->setPromotion(null),
            (new Produit())->setId(3)->setNom('Fanta')->setPrix(0.75)->setImage('https://yourspanishcorner.com/6660-thickbox_default/fanta-orange-canette-33-cl.jpg')->setPromotion(25),
            (new Produit())->setId(4)->setNom('Dr Pepper')->setPrix(1)->setImage('https://www.myamericanmarket.com/977-large_default/dr-pepper-soda.jpg')->setPromotion(null),
            (new Produit())->setId(5)->setNom('Nalu')->setPrix(1.3)->setImage('https://cdn.carrefour.eu/1200_05563975_5060466511859_02.jpeg')->setPromotion(null),
        ];
    }

    /**
     * @Route("/produit/correction", name="produit_correction")
     */
    public function index(): Response
    {
        return $this->render('produit_correction/index.html.twig', [
            'produits' => $this->db
        ]);
    }

    /**
     * @Route("/produit/details/{id}", name="produit_details", requirements={"id": "\d+"})
     */
    public function details($id) {

        $list = array_filter($this->db, function($item) use($id) {
            return $item->getId() == $id;
        });
        if(empty($list)) {
            throw new NotFoundHttpException();
        }
        else {
            return $this->render('produit_correction/details.html.twig', [
                'produit' => array_shift($list)
            ]);
        }
    }
}
